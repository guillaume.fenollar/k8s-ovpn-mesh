#!/bin/bash

set -e

SERVERS='192.168.110.40:prod:10.42.1.0
192.168.110.50:qual:10.42.2.0
192.168.60.40:kone:10.42.3.0'

dockercmd='docker run --net=none --rm -it -v $PWD:/etc/openvpn -w /etc/openvpn'
dockerimg=kylemanna/openvpn

function initpki() {
  # Generation de la PKI
  eval $dockercmd $dockerimg easyrsa init-pki
  eval $dockercmd $dockerimg easyrsa build-ca nopass
  eval $dockercmd $dockerimg easyrsa gen-dh
  eval $dockercmd $dockerimg openvpn --genkey --secret pki/ta.key
}

function writeClientConf() {
  dst=$clientenv/client.conf
  cat openvpn-client.tpl > $dst
  sed -i "s|__ENDPOINT__|$endpoint|" $dst
  cat pki/private/$client.key >> $dst
  echo -e '</key>\n<cert>'  >> $dst
  openssl x509 -in pki/issued/$client.crt >> $dst
  echo -e '</cert>\n<ca>'  >> $dst
  cat pki/ca.crt >> $dst
  echo -e '</ca>\n<tls-auth>'  >> $dst
  cat pki/ta.key >> $dst
  echo -e '</tls-auth>'  >> $dst
}

test -f pki/ta.key || initpki

for s in $SERVERS; do
  # vars
  endpoint=$(echo $s|cut -d':' -f1)
  env=$(echo $s|cut -d':' -f2)
  subnet=$(echo $s|cut -d':' -f3)

  mkdir -p $env
  # Generation de tous les certifs et clés

  ## SERVER
  test -f pki/issued/$endpoint.crt || \
      eval $dockercmd $dockerimg \
        easyrsa build-server-full $endpoint nopass
  kubectl create ns --dry-run -oyaml ovpn > $env/0ns.yaml
  ## Generation conf openvpn.conf
  sed -e "s|__ENDPOINT__|$endpoint|" -e "s|__SUBNET__|$subnet|" \
	  openvpn-server.tpl > $env/openvpn.conf
  cp ovpn_env.sh $env/ovpn_env.sh
  kubectl create cm openvpn-conf --from-file $env/openvpn.conf --from-file $env/ovpn_env.sh -oyaml --dry-run > $env/openvpn.conf.yaml \
	  && rm -f $env/openvpn.conf && rm -f $env/ovpn_env.sh
  ## Ecriture secrets
  kubectl create secret generic openvpn-server --dry-run -oyaml --from-file=pki/ca.crt --from-file=pki/dh.pem --from-file=pki/ta.key --from-file=pki/issued/${endpoint}.crt --from-file=pki/private/${endpoint}.key > $env/secrets.yaml
  ## Ecriture DS k8s serveur
  cp ovpn-server-daemonset.yaml $env


  ## CLIENT
  for c in $SERVERS;  do
    clientenv=$(echo $c|cut -d':' -f2)
    if [[ $clientenv != $env ]]; then
      client=${clientenv}2${env}
      if [[ ! -f pki/issued/${client}.crt ]]; then
        eval $dockercmd $dockerimg easyrsa build-client-full ${client} nopass
      fi
      mkdir -p $clientenv
      sed "s|__CLIENT__|$client|" ovpn-client-daemonset.yaml > $clientenv/${client}-ds.yaml
      writeClientConf
      kubectl create secret generic $client --from-file $clientenv/client.conf -oyaml --dry-run > $clientenv/${client}.conf.yaml \
	        && rm -r $clientenv/client.conf
    fi
  done

done

echo Generation terminée avec succès
