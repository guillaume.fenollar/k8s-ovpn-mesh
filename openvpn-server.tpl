server __SUBNET__ 255.255.255.0
verb 3
key /etc/openvpn/pki/__ENDPOINT__.key
ca /etc/openvpn/pki/ca.crt
cert /etc/openvpn/pki/__ENDPOINT__.crt
dh /etc/openvpn/pki/dh.pem
tls-auth /etc/openvpn/pki/ta.key
key-direction 0
keepalive 10 60
persist-key
persist-tun

proto udp
# Rely on Docker to do port mapping, internally always 1194
port 1194
dev tun
status /tmp/openvpn-status.log

user nobody
group nogroup
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-256-GCM-SHA384
cipher AES-256-GCM
auth SHA384
comp-lzo no

### Route Configurations Below
#route __SUBNET__ 255.255.255.0

### Push Configurations Below
#push "route 192.168.16.0 255.255.240.0"
#push "route 192.168.32.0 255.255.240.0"
push "route __SUBNET__ 255.255.255.0"

duplicate-cn
